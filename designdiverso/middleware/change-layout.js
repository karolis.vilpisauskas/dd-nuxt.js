export default function(ctx) {
  const slug = ctx.params.slug
  if (slug === 'about' || slug === 'thoughs') {
    ctx.isDark = true
  } else {
    ctx.isDark = false
  }
}
