import axios from 'axios'

const state = () => ({
  media: [],
  single: {}
})

const getters = {
  allMedia: state => state.media,
  singleMedia: state => state.single
}

const actions = {
  async getMedia({ commit }) {
    const response = await axios.get('http://designwp.carl/wp-json/wp/v2/media')
    commit('setMedia', response.data)
  },
  async getSingle({ commit }, id) {
    const response = await axios.get(
      `http://designwp.carl/wp-json/wp/v2/media/${id}`
    )
    commit('setSingle', response.data)
  }
}

const mutations = {
  setMedia: (state, media) => (state.media = media),
  setSingle: (state, single) => (state.single = single)
}

export default {
  state,
  getters,
  actions,
  mutations
}
