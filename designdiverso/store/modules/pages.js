import axios from 'axios'

const state = () => ({
  pages: [],
  page: {}
})

const getters = {
  Pages: state => state.pages,
  Page: state => state.page
}

const actions = {
  async getPages({ commit }) {
    const res = await axios.get('http://designwp.carl/wp-json/wp/v2/pages')
    commit('setPages', res.data)
  },
  async getPage({ commit }, slug) {
    const res = await axios.get(
      `http://designwp.carl/wp-json/wp/v2/pages?slug=${slug}`
    )
    commit('setPage', res.data)
  }
}

const mutations = {
  setPages: (state, pages) => (state.pages = pages),
  setPage: (state, page) => (state.page = page)
}

export default {
  state,
  getters,
  actions,
  mutations
}
