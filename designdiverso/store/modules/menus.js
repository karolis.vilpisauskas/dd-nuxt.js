import axios from 'axios'

const state = () => ({
  menus: [],
  menu: {}
})

const getters = {
  Menus: state => state.menus,
  Menu: state => state.menu
}

const actions = {
  async getMenus({ commit }) {
    const response = await axios.get(
      'http://designwp.carl/wp-json/menus/v1/menus/'
    )
    commit('setMenus', response.data)
  },
  async getMenu({ commit }, id) {
    const response = await axios.get(
      `http://designwp.carl/wp-json/menus/v1/slugs/`
    )
    commit('setMenu', response.data)
  }
}

const mutations = {
  setMenus: (state, menus) => (state.menus = menus),
  setMenu: (state, menu) => (state.menu = menu)
}
export default {
  state,
  getters,
  actions,
  mutations
}
