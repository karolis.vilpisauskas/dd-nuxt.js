//GLOBAL
const $owl = $('.owl-carousel') ;

$(document).ready(function() {

    // Toggle nav
    var $nav_toggle = $('#nav-toggle');
    var $nav_main = $('#nav-main');
    var $menu = $('#nav-menu');
    $nav_toggle.click(function() {
      $(this).toggleClass('is-active');
      $nav_main.toggleClass('dd-is-active');
      $menu.toggleClass('is-active');
    });

    // Scroll to bottom of hero
    $('.scrollDown').click(function() {
      $('html, body').animate({scrollTop: $(".hero").height()}, 500);
      return false;
    });

    // Headroom settings
    $("nav[role='navigation']").headroom({
      offset: 70
    });

    // Launch modal
    $('.modal-button').click(function() {
      var target = $(this).data('target');
      $('html').addClass('is-clipped');
      $(target).addClass('is-active');
    });

    // Close modal
    $('.modal-background, .modal-close').click(function() {
      $('html').removeClass('is-clipped');
      $(this).parent().removeClass('is-active');
    });

    $('.modal-card-head .delete, .modal-card-foot .button').click(function() {
      $('html').removeClass('is-clipped');
      $('#modal-ter').removeClass('is-active');
    });

    $(document).on('keyup',function(e) {
      if (e.keyCode == 27) {
        $('html').removeClass('is-clipped');
        $('.modal').removeClass('is-active');
      }
    });

    // Responsive iframe
    // Find all YouTube videos
    var $iframes = $("iframe");
    // Figure out and save aspect ratio for each video
    $iframes.each(function() {
      var height = $(this).height();
      var width = $(this).width();
      $(this)
        .data('ratio', height / width)
        // and remove the hard coded width/height
        .removeAttr('height')
        .removeAttr('width');
    });
    // When the window is resized
    $(window).resize(function() {
      // Resize all videos according to their own aspect ratio
      $iframes.each(function() {
        var width = $(this).parent().width();
        $(this)
          .width(width)
          .height(width * $(this).data('ratio'));
      });
    // Kick off one resize to fix all videos on page load
    }).resize();


});

